export const menuList: Array<any> = [
	{
		title: 'Button按钮',
		path: '../button/Button'
	},
	{
		title: 'Icon图标',
		path: '../icon/Icon'
	},
	{
		title: 'Card卡片',
		path: '../card/Card'
	},
	{
		title: 'Tag标签',
		path: '../tag/Tag'
	},
	{
		title: 'List列表',
		path: '../list/List'
	},
	{
		title: 'Swiper轮播',
		path: '../swiper/Swiper'
	},
	{
		title: 'Form表单',
		path: '../form/Form'
	},
	{
		title: 'Navbar导航',
		path: '../navbar/NavBar'
	},
	{
		title: 'Tabs标签页',
		path: '../tabs/Tabs'
	},
	{
		title: 'Badge数字角标',
		path: '../badge/Badge'
	},
	{
		title: 'Message消息通知',
		path: '../message/Message'
	},
	{
		title: 'Transition动画',
		path: '../transition/Transition'
	},
	{
		title: 'Text文本',
		path: '../text/Text'
	},
	{
		title: 'Avatar头像',
		path: '../avatar/Avatar'
	},
	{
		title: 'Empty空状态',
		path: '../empty/Empty'
	},
	{
		title: 'Image图片',
		path: '../image/Image'
	},
	{
		title: 'Layout布局',
		path: '../layout/Layout'
	},
	{
		title: 'LoadingMore加载更多',
		path: '../loading/Loading'
	},
	{
		title: 'Process进度条',
		path: '../process/Process'
	},
	{
		title: 'Popup弹出层',
		path: '../popup/Popup'
	},
	{
		title: 'ActionSheet动作面板',
		path: '../actionSheet/ActionSheet'
	},
	{
		title: 'Dialog确认框',
		path: '../dialog/Dialog'
	},
	{
		title: 'Toast轻提示',
		path: '../toast/Toast'
	},
	{
		title: 'ShareSheet分享面板',
		path: '../shareSheet/ShareSheet'
	},
	{
		title: 'Divider分割线',
		path: '../divider/Divider'
	},
	{
		title: 'Grid宫格',
		path: '../grid/Grid'
	},
	{
		title: 'Steps步骤条',
		path: '../steps/Steps'
	},
	{
		title: 'Pagination分页',
		path: '../pagination/Pagination'
	},
	{
		title: 'Tabbar导航栏',
		path: '../tabbar/Tabbar'
	},
	{
		title: 'Search搜索框',
		path: '../search/Search'
	},
	{
		title: 'TimeLine时间线',
		path: '../timeline/TimeLine'
	},
	{
		title: 'Collapse折叠面板',
		path: '../collapse/Collapse'
	}
	// {
	// 	title:"NoticeBar通知",
	// 	path:"../noticebar/NoticeBar"
	// }
];
