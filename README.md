<h3 align="center" style="margin: 30px 0 30px;font-weight: bold;font-size:40px;">Mural Design</h3>
<h3 align="center">全端通用的UI组件库</h3>

## 说明

基于[uni-app](https://uniapp.dcloud.io/)生态的UI框架，便捷工具开发，通过[uni_modules](https://uniapp.dcloud.net.cn/plugin/uni_modules.html)导入使用

## 预览

可以通过下方扫码体验
<br>
<br>

#### 微信小程序

<p>
	<img src="https://images.elegants.design/mural/qrcode.png" width="180" height="180">
</p>

#### H5

<p>
	<img src="https://images.elegants.design/mural/h5code.png" width="180" height="180">
</p>

[跳转H5端实例](https://h5.elegants.design/)

## 使用方式

通过[https://ext.dcloud.net.cn/plugin?name=mural-design](https://ext.dcloud.net.cn/plugin?name=mural-design)导入组件，无需`import`组件，直接引用即可

```html
<template>
	<mu-button>按钮</mu-button>
</template>
```