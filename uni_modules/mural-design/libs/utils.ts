export const transitionType = (name: string, position?: string) => {
	return position && position.length >= 0 ? name + '-' + position : name;
};

export const getWindowInfo = () => {
	return uni.getWindowInfo();
};
