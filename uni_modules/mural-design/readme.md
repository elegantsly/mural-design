## 使用方法

导入插件后，无需import组件，直接引用即可。

```html
<template>
	<mu-button></mu-button>
	<mu-button type="primary"></mu-button>
	<mu-button type="primary" plain></mu-button>
</template>
```